<?php
$lang_mod_vsabr = array(
 'Robot title' => 'Вы человек или робот?',
 'Robot question' => 'Пожалуйста, ответьте на следующий вопрос (все символы в нижнем регистре): <b>%s</b>',
 'Robot info' => 'Проверка запроса реального ли человека или робота.',
 'Robot test fail' => 'Ваш ответ неправильный на запрос \'Человек или робот\', вы бот!'
);
$mod_vsabr_questions = array(
 'Complete the slogan: "software freedom ..."' => 'your way',
 'What is the codename of the current Devuan stable release?' => 'ascii',
 'What does the "U" in VUA stand for?' => 'unix',
 'What is the name of the Devuan unstable suite?' => 'ceres',
 'Devuan is a fork of what other distribution?' => 'debian',
 'What is the name of the Xfce file manager?' => 'thunar',
 'What is the last name of the creator of Linux?' => 'torvalds',
 'What is the default Desktop Environment for Devuan?' => 'xfce',
 'What are the first five prime numbers? (Do not use commas to separate the numbers)' => '2 3 5 7 11',
 'What is the name of the Devuan release coming after ASCII?' => 'beowulf',
 'What does SSD stand for?' => 'solid state drive' 
);
?>
