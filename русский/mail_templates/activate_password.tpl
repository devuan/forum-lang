Subject: Запрос изменения пароля

Привет <username>,

Вы отправили запрос для изменения пароля, привязанного к Вашей учётной записи на форуме <base_url>. Если Вы не запрашивали данное действие или не хотите менять ваш пароль, то просто проигнорируйте данное сообщение. Ваш пароль будет изменён только после посещения страницы активации.

Ваш новый пароль: <new_password>
Для смены Вашего пароля, пожалуйста, перейдите по следующей ссылке:
<activation_url>

--
<board_mailer> Отправитель
(Не отвечайте на данное сообщение)
