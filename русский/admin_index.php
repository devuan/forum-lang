<?php
$lang_admin_index = array(
 'fopen disabled message' => 'Невозможно выполнить перенаправление \'allow_url_fopen\' - это отключено в вашей системе.',
 'Upgrade check failed message' => 'Проверка ошибки перенаправления для неизвестных ответов.',
 'Running latest version message' => 'Вы используете последнюю версию FluxBB.',
 'New version available message' => 'Вышла новая версия FluxBB. Вы можете загрузить новую версию используя %s.',
 'Deleted install.php redirect' => 'Файл успешно удалён, перенаправление ...',
 'Delete install.php failed' => 'Вы не можете удалить install.php. Вы должны это сделать вручную.',
 'Not available' => 'Не доступно',
 'Forum admin head' => 'АДминистрирование форума',
 'NA' => 'Не определено',
 'Welcome to admin' => 'Добро пожаловать в панель администрирования FluxBB. Отсюда вы можете контролировать многие аспекты функциональности форума. В зависимости от того, являетесь ли Вы администратором или модератором, Вы можете:',
 'Welcome 1' => 'Организация категорий и форумов.',
 'Welcome 2' => 'Установка опций и настроек форума.',
 'Welcome 3' => 'Управление разрешениями для пользователей и гостей.',
 'Welcome 4' => 'Просмотр IP статистики попользователям.',
 'Welcome 5' => 'Заблокированные пользователия',
 'Welcome 6' => 'Цензурируемые слова.',
 'Welcome 7' => 'Настройка групп пользователей и продвижения.',
 'Welcome 8' => 'Сократите старые посты.',
 'Welcome 9' => 'Почтовые отчёты',
 'Alerts head' => 'События',
 'Install file exists' => 'Файл install.php существует и должен быть удалён. %s.',
 'Delete install file' => 'Удалить это',
 'About head' => 'Об FluxBB',
 'FluxBB version label' => 'Версия FluxBB',
 'Check for upgrade' => 'Проверка наличия обновлений',
 'FluxBB version data' => 'v%s - %s',
 'Server statistics label' => 'Статистика сервера',
 'View server statistics' => 'Просмотр статистики сервера',
 'Support label' => 'Поддержка',
 'Forum label' => 'Форум',
 'IRC label' => 'IRC канал',
 'PHPinfo disabled message' => 'Функция phpinfo() отключена на данном сервере.',
 'Server statistics head' => 'Статистика сервера',
 'Server load label' => 'Нагрузка сервера',
 'Server load data' => '%s - %s пользователь(-ей) онлайн',
 'Environment label' => 'Переменные',
 'Environment data OS' => 'Оперционная система: %s',
 'Show info' => 'Просмотр информации',
 'Environment data version' => 'PHP: %s - %s',
 'Environment data acc' => 'Акселератор: %s',
 'Turck MMCache' => 'Turck MMCache',
 'Turck MMCache link' => 'turck-mmcache.sourceforge.net/',
 'ionCube PHP Accelerator' => 'ionCube PHP акселератор',
 'ionCube PHP Accelerator link' => 'www.php-accelerator.co.uk/',
 'Alternative PHP Cache (APC)' => 'Альтернативный PHP кеш (APC)',
 'Alternative PHP Cache (APC) link' => 'www.php.net/apc/',
 'Zend Optimizer' => 'Zend Optimizer',
 'Zend Optimizer link' => 'www.zend.com/products/guard/zend-optimizer/',
 'eAccelerator' => 'eAccelerator',
 'eAccelerator link' => 'www.eaccelerator.net/',
 'XCache' => 'XCache',
 'XCache link' => 'xcache.lighttpd.net/',
 'Database label' => 'База данных',
 'Database data rows' => 'Строк: %s',
 'Database data size' => 'Размер: %s'
);
?>
