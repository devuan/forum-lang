Asunto: Alerta - Correo expulsado detectado

El usuario '<username>' se ha registrado con una dirección de correo electrónico expulsada: <email>

Perfil del usuario: <profile_url>

--
<board_mailer> Remitente
(No respondas a este mensaje)
