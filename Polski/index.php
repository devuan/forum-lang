<?php
$lang_index = array(
 'Topics' => 'Wątki',
 'Link to' => 'Link do:',
 'Empty board' => 'Forum jest puste.',
 'Newest user' => 'Ostatnio zarejestrowany: %s',
 'Users online' => 'Użytkowników online: %s',
 'Guests online' => 'Gości online: %s',
 'No of users' => 'Zarejestrowanych użytkowników: %s',
 'No of topics' => 'Wszystkich wątków: %s',
 'No of posts' => 'Wszystkich postów: %s',
 'Online' => 'Online:',
 'Board info' => 'Informacje o forum',
 'Board stats' => 'Statystyki forum',
 'User info' => 'Informacje o użytkowniku'
);
?>
