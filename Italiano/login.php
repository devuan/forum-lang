<?php
$lang_login = array(
 'Login errors' => 'Login error',
 'Login errors info' => 'The following error needs to be corrected before you can login:',
 'Wrong user/pass' => 'Nome utente o password errati.',
 'Forgotten pass' => 'Password dimenticata?',
 'Login redirect' => 'Accesso effettuato. Reindirizzamento &hellip;',
 'Logout redirect' => 'Disconnessione effettuata. Reindirizzamento &hellip;',
 'No email match' => 'Non ci sono utenti registrati con questo indirizzo email',
 'Request pass' => 'Richiesta password',
 'Request pass legend' => 'Inserire l\'indirizzo email di registrazione al forum',
 'Request pass info' => 'Una nuova password e un collegamento di attivazione sono inviati all\'indirizzo email indicato.',
 'Not registered' => 'Registrati!',
 'Login legend' => 'Inserire nome utente e password',
 'Remember me' => 'Accesso automatico.',
 'Login info' => 'Informazioni di accesso al forum.',
 'New password errors' => 'Errore',
 'New passworderrors info' => 'Correggere i seguenti errori per ricevere una nuova passord:',
 'Forget mail' => 'Un\'email &egrave; stata inviata all\'indirizzo indicato con le istruzioni necessarie. Qualora non arrivasse &egrave; possibile contattare l\'amministratore del forum ',
 'Email flood' => 'Questo utente ha richiesto una nuova password durante l\'ultima ora. Si prega di attendere %s minuti e provare nuovamente.',
 'Login at dev1galaxy' => 'Login at dev1galaxy.org below',
 'OR Login via' => 'OR Login via',
 'Login via git.devuan.org' => 'git.devuan.org'
);
?>
