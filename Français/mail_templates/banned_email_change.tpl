Object: Avertissement - courriel banni détecté

L'utilisateur « <username> » a modifié son adresse de courrier électronique pour l'adresse bannie suivante : <email>

Profil utilisateur : <profile_url>

-- 
<board_mailer>
(Veuillez ne pas répondre à ce message. Merci !)
