Objet: Avertissement - courriel banni détecté

L'utilisateur « <username> » s'est inscrit avec l'adresse courriel bannie suivante : <email>

Profil utilisateur : <profile_url>

-- 
<board_mailer>
(Veuillez ne pas répondre à ce message. Merci !)
