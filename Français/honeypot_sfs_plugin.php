<?php
$lang_honeypot_sfs_plugin = array(
 'Description' => 'Ce plugin sert à contrôler les réglages des modes Honeypot + StopForumSpam.',
 'Options' => 'Options',
 'Settings' => 'Réglages',
 'StopForumSpam check label' => 'Vérification StopForumSpam',
 'StopForumSpam check help' => 'Si l\'utilisateur essayant de s\'inscrire passe outre la vérification honeypot, vérifier son adresse IP et son adresse email (et non son nom d\'utilisateur) sur la liste noire de la base de donnée StopForumSpam. Honeypot est réputé bloquer près de 100% des bots, le service StopForumSpam est un second rempart contre les spammers humains.',
 'StopForumSpam API label' => 'API StopForumSpam',
 'StopForumSpam API help' => 'Votre clé pour l\'API StopForumSpam. Si le fichier est laissé vide, les blocages des tentatives de spam ne seront pas signalées au service de liste noire de StopForumSpam.',
 'Options updated redirect' => 'Options actualisées. Redirection...',
 'Search users head' => 'Recherche utilisateurs',
 'Search users info' => 'Cette fonctionnalité permet de rechercher des utilisateurs par les URLs inclus dans leurs signatures avec 0 posts.  Cela permet de trouver des spammers ayant réussi à passer outre les vérifications Honeypot et StopForumSpam. Les résultats de recherche se limitent aux 50 derniers utilisateurs enregistrés répondant aux critères.',
 'Statistics' => 'Statistiques',
 'Collecting stats since label' => 'Collecte des statistiques depuis',
 'Num days' => '%s jours',
 'Not available' => 'Non disponible',
 'Total label' => 'Total',
 'Average last 7 days label' => 'Moyenne 7 derniers jours',
 'Maximum day label' => 'Jour maximum',
 'Blocked last 14 days label' => 'Bloqués ces 14 derniers jours',
 'Not spam info' => 'Non spam: %s',
 'Blocked by Honeypot info' => 'Bloqués par Honeypot: %s',
 'Blocked by SFS info' => 'Bloqués par SFS: %s',
 'per day' => 'par jour',
 'Date' => 'Date',
 'Total' => 'Total'
);
?>
