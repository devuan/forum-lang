<?php
$lang_index = array(
 'Topics' => 'Trådar',
 'Link to' => 'Link to:',
 'Empty board' => 'Board is empty.',
 'Newest user' => 'Nyaste medlem: %s',
 'Users online' => 'Antal medlemmar online: %s',
 'Guests online' => 'Gäster online: %s',
 'No of users' => 'Totalt registrerade medlemmar: %s',
 'No of topics' => 'Totalt trådar: %s',
 'No of posts' => 'Totalt inlägg: %s',
 'Online' => 'Inloggade:',
 'Board info' => 'Forum information',
 'Board stats' => 'Forum statistik',
 'User info' => 'Användarinformation',
);
?>
