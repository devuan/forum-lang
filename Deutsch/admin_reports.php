<?php

// Language definitions used in admin_reports.php
$lang_admin_reports = array(

 'Report zapped redirect' => 'Berichte als gelesen markiert. Weiterleitung …',
 'New reports head' => 'Neue Berichte',
 'Deleted user' => 'Gelöschte Benutzer',
 'Deleted' => 'Gelöscht',
 'Post ID' => 'Beitrag #%s',
 'Report subhead' => 'Berichtet %s',
 'Reported by' => 'Berichtet von %s',
 'Reason' => 'Grund',
 'Zap' => 'Als gelesen markiert',
 'No new reports' => 'Keine neuen Berichte.',
 'Last 10 head' => '10 letzten gelesenen Berichte',
 'NA' => 'nicht verfügbar',
 'Zapped subhead' => 'Als gelesen markiert %s von %s',
 'No zapped reports' => 'Keine gelesenen Berichte.',

);
