<?php
$lang_ul = array(
 'User find legend' => 'Suche und sortiere Benutzer',
 'User search info' => 'Gib den Benutzernamen ein, nach dem gesucht werden soll und/oder eine Benutzergruppe, nach der gefiltert wird. Der Benutzername kann dann leer bleiben. Benutze den Platzhalter *, um nach Teilbegriffen zu suchen.',
 'User sort info' => 'Sortiert Benutzer nach Namen, Registrierungsdatum oder Anzahl der Beiträge in aufsteigender/absteigender Reihenfolge.',
 'User group' => 'Benutzergruppe',
 'No of posts' => 'Anzahl der Beiträge',
 'All users' => 'Alle'
);
?>
