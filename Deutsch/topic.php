<?php

// Language definitions used in viewtopic.php
$lang_topic = array(

 'Post reply' => 'Antwort abschicken',
 'Topic closed' => 'Thema geschlossen',
 'From' => 'Von:', // User location
 'Promote user' => 'Benutzer unterstützen',
 'IP address logged' => 'IP Adresse aufgezeichnet',
 'Note' => 'Bemerkung:', // Admin note
 'Posts' => 'Beiträge:',
 'Registered' => 'Registriert:',
 'Replies' => 'Antworten:',
 'Website' => 'Webseite',
 'Guest' => 'Gast',
 'Online' => 'Online',
 'Offline' => 'Offline',
 'Last edit' => 'Zuletzt geändert von',
 'Report' => 'Bericht',
 'Delete' => 'Löschen',
 'Edit' => 'Editieren',
 'Quote' => 'Zitat',
 'Is subscribed' => 'Du hast dieses Thema abbonniert',
 'Unsubscribe' => 'Abmelden',
 'Subscribe' => 'Abonniere dieses Thema',
 'Quick post' => 'Schnelle Antwort',
 'Mod controls' => 'Moderator Kontrolle',
 'New icon' => 'Neuer Beitrag',
 'Re' => 'Re:',
 'Preview' => 'Vorschau'

);
