Betreff: Neuer Benutzername

Anlässlich eines Upgrades des Forums <base_url> wurde festgestellt, dass dein Benutzername dem Namen eines anderen Benutzers zu ähnlich ist. Dein Benutzername wurde deshalb geändert.

Alter Benutzername: <old_username>
Neuer Benutzername: <new_username>

Bitte entschuldige die Unannehmlichkeiten, die dir dadurch entstehen.

--
<board_mailer> Mailer
(Nicht auf diese Nachricht antworten)
