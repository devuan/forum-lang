Betreff: Antwort im Thema: '<topic_subject>'

<replier> hat im Thema '<topic_subject>' ,das Du abonniert hast, geantwortet. Es gibt möglicherweise weitere neue Beiträge dazu, aber dies ist die einzige Nachricht, die Du dazu erhältst, bis Du das Form das nächste mal besuchst.

Link zum Beitrag <post_url>

Der Text des Beitrages lautet wie folgt:
-----------------------------------------------------------------------

<message>

-----------------------------------------------------------------------

Du kannst dein Abonnement dieses Themas beenden, indem Du die Seite <unsubscribe_url> besuchst und dort auf den Link "Abonnement beenden" unten auf der Seite klickst.

--
<board_mailer> Mailer
(Nicht auf diese Nachricht antworten)
