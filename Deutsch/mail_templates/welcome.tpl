Betreff: Willkommen auf <board_title>!

Vielen Dank für deine Registrierung in unseren Foren <base_url>. Hier die Daten zu deinem Account:

Benutzername: <username>
Passwort: <password>

Logge dich hier <login_url> ein, um deinem Account zu aktivieren.

--
<board_mailer> Mailer
(Nicht auf diese Nachricht antworten)
