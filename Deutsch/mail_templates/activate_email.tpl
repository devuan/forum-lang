Betreff: Änderung der Email Adresse beantragt

Hallo <username>,

Du hast eine neue Email Adresse für dein Konto im Diskussions-Forum <base_url> beantragt. Kommt dieser Antrag nicht von dir oder wünscht Du keine neue Email-Adresse, so kannst Du diese Nachricht ignorieren. Deine Email wird erst geändert, nachdem Du die untenstehende Aktivierungsseite besucht hast. Damit die Aktivierung funktioniert, musst Du im Forum eingeloggt sein.

Folge diesem Link, um deine Email Adresse zu ändern:
<activation_url>

--
<board_mailer> Mailer
(Nicht auf diese Nachricht antworten)
