Betreff: Neues Thema im Forum: '<forum_name>'

<poster> hat ein neues Thema '<topic_subject>' im Forum '<forum_name>' , das Du abonniert hast, eröffnet.

Link zum Thema <topic_url>

Du kannst dein Abonnement beenden, indem Du auf <unsubscribe_url> den Link "Abonnement löschen" unten auf der Seite aufrufst.

--
<board_mailer> Mailer
(Nicht auf diese Nachricht antworten)
