Betreff: Antwort im Thema '<topic_subject>'

<replier> hat im Thema '<topic_subject>', das Du abonniert hast, einen Beitrag geschrieben. Möglicherweise gibt es weitere neue Beiträge, aber dies ist die einzige Benachrichtigung dazu, bis Du das Forum erneut besuchst.

Der neue Beitrag <post_url>

Du kannst dein Abonnement des Themas beenden, indem Du <unsubscribe_url> besuchst und dort auf den Link "Abonnement beenden" unten auf der Seite klickst.

--
<board_mailer> Mailer
(Nicht auf diese Nachricht antworten)
