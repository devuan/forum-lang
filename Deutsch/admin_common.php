<?php

// Language definitions used in admin_common.php
$lang_admin_common = array(

// The menu
 'Admin menu' => 'Admin Menü',
 'Plugins menu' => 'Plugin Menü',
 'Moderator menu' => 'Moderator Menü',
 'Index' => 'Index',
 'Categories' => 'Kategorien',
 'Forums' => 'Foren',
 'Users' => 'Benutzer',
 'User groups' => 'Benutzer Gruppen',
 'Options' => 'Optionen',
 'Permissions' => 'Berechtigungen',
 'Censoring' => 'Zensieren',
 'Bans' => 'Sperren',
 'Prune' => 'Auskehren',
 'Maintenance' => 'Wartung',
 'Reports' => 'Berichte',
 'Server statistics' => 'Server Statistiken',

 'Admin' => 'Admin',
 'Go back' => 'Zurück',
 'Delete' => 'Löschen',
 'Update' => 'Aktualisieren',
 'Add' => 'Hinzufügen',
 'Edit' => 'Bearbeiten',
 'Remove' => 'Entfernen',
 'Yes' => 'Ja',
 'No' => 'Nein',
 'Save changes' => 'Änderungen speichern',
 'Save' => 'Speichern',
 'here' => 'hier',
 'Action' => 'Aktion',
 'None' => 'Keine',
 'Maintenance mode' => 'Wartungs Modus', // Used for link text in more than one file

// Admin loader
 'No plugin message' => 'Ein Plugin mit dem Namen %s gibt es im Plugin-Verzeichnis nicht.',
 'Plugin failed message' => 'Laden das Plugin - <strong>%s</strong> - gescheitert.',

);
