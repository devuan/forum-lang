<?php
$lang_honeypot_sfs_plugin = array(
 'Description' => 'Dieses Plugin dient der Kontrolle und Einstellung des Honigtopfes und des StopForumSpam Moduls.',
 'Options' => 'Optionen',
 'Settings' => 'Einstellungen',
 'StopForumSpam check label' => 'StopForumSpam Check',
 'StopForumSpam check help' => 'Wenn der Benutzer beim Versuch der Registrierung den Honigtopf umgangen hat, überprüfe seine IP Adresse und Email Addresse (nicht den Benutzernamen) gegen die Schwarze Liste (blacklist) der StopForumSpam Datenbak. Während der Honigtopf nahezu 100%ig gegen Bots schützt, stellt der StopForumSpam Service eine zweite Barriere gegen menschliche Spammer dar.',
 'StopForumSpam API label' => 'StopForumSpam API',
 'StopForumSpam API help' => 'Dein Schlüssel für die StopForumSpam API. Wird dieses Feld leer gelassen, werden blockierte Spams-Versuche nicht in der Schwarzen Liste des StopForumSpam Service erfasst.',
 'Options updated redirect' => 'Optionen aktualisiert. Weiterleitung …',
 'Search users head' => 'Benutzer Suche',
 'Search users info' => 'Diese Eigenschaft ermöglicht es, nach Benutzern zu suchen, die URLs in ihrer Signature, aber noch Beiträge haben. Das kann auf Spammer hinweisen, die erfolgreich den Honigtopf und den StopForumSpam Check umgangen haben. Die Suchergebnisse sind reduziert auf die letzten 50 registrierten Benutzer, auf die dieses Kriterium zutrifft.',
// Statistics
 'Statistics' => 'Statistiken',
 'Collecting stats since label' => 'Gesammelte Statistiken seit',
 'Num days' => '%s Tagen',
 'Not available' => 'Nicht verfügbar',
 'Total label' => 'Total',
 'Average last 7 days label' => 'Durchschnitt der letzten 7 Tage',
 'Maximum day label' => 'Maximum pro Tag',
 'Blocked last 14 days label' => 'Blockiert Zugriffsversuche der letzten 14 Tage',
 'Not spam info' => 'Kein Spam: %s',
 'Blocked by Honeypot info' => 'Blockiert vom Honigtopf: %s',
 'Blocked by SFS info' => 'Blockiert von SFS: %s',
 'per day' => 'pro Tag',
 'Date' => 'Datum',
 'Total' => 'Total',
);
?>
