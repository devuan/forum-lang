#!/bin/bash
#
# Extract and cleanup the mapping

for F in $* ; do
    sed "s/[^']*\('[^']*'\)"'[ 	]*=>[ 	]*\(.*\)/ \1 => \2/' -i $F
done
